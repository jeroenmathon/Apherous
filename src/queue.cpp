//
// Created by jeroen on 9/28/15.
//
#include "queue.h"
/**
 *****************************************************************************************
 *  @brief      Adds a uri to the audioQueue
 *
 *  @usage      Inserts a uri into the audioQueue vector of queueStruct
 *
 *  @param      std::string uri: The uri to add into the audioQueue
 ******************************************************************************************
 **/
void queue::add(std::string uri)
{
    // Assemble uri
    std::stringstream path;
    path << "file://" << uri;

    // Check if the uri points to an valid file
    std::ifstream tmpFile(uri);
    if(tmpFile.good()) uriQueue.push_back(path.str());
    else
    {
        std::cout << "[ERROR]:File not found or could not access file.\n";
    }
}

/**
 *****************************************************************************************
 *  @brief      Increments currentQueueItem
 *
 *  @usage      Increments currentQueueItem if its not larger that the audioQueue size
 ******************************************************************************************
 **/
void queue::next()
{
    if(currentQueueItem+1 < uriQueue.size()) currentQueueItem++;
}

/**
 *****************************************************************************************
 *  @brief      Decrements currentQueueItem
 *
 *  @usage      Decrements currentQueueItem if its not smaller than 0
 ******************************************************************************************
 **/
void queue::prev()
{
    if(currentQueueItem > 0) currentQueueItem--;
}

/**
 *****************************************************************************************
 *  @brief      Gets the current uri
 *
 *  @usage      Retrieves the current uri from the queueStruct uriQueue vector
 ******************************************************************************************
 **/
std::string queue::getCurrentQueueItem()
{
    if(uriQueue.size() > 0) return uriQueue[currentQueueItem];
}

/**
 *****************************************************************************************
 *  @brief      Returns the size of the audioQueue
 ******************************************************************************************
 **/
int queue::size()
{
    return uriQueue.size();
}

/**
 *****************************************************************************************
 *  @brief      Returns the current position of the audioQueue
 ******************************************************************************************
 **/
int queue::at()
{
    return currentQueueItem;
}