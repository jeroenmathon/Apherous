//
// Created by jeroen on 9/26/15.
//

#include "ui_gtk.h"

gtkInterface::gtkInterface()
{
    // Set up the main window and box
    builder->get_widget("window", window);
    builder->get_widget("mainBox", mainBox);

    // Set up the Filebox
    builder->get_widget("fileBox", fileBox);
    builder->get_widget("browseBtn", browseBtn);
    builder->get_widget("addFileBtn", addFileBtn);
    builder->get_widget("pathEntry", pathEntry);

    // Set up the Controlbox
    builder->get_widget("controlBox", controlBox);
    builder->get_widget("playBtn", playBtn);
    builder->get_widget("pauseBtn", pauseBtn);
    builder->get_widget("stopBtn", stopBtn);
    builder->get_widget("prevBtn", prevBtn);
    builder->get_widget("nextBtn", nextBtn);
    builder->get_widget("progressBar", progressBar);

    // Set up the CurrentSongBox
    builder->get_widget("CurrentSongBox", currentSongBox);
    builder->get_widget("currentSongText", currentSongText);
    builder->get_widget("CurrentSong", currentSong);

    // Set up the FileChooserBox
    builder->get_widget("filechooserdialog1", fileChooserDialog1);
    builder->get_widget("filechooserdialog-vbox1", fileChooserDialogVbox);
    builder->get_widget("filechooserdialog-action_area1", fileChooserDialogActionArea1);
    builder->get_widget("filechooser_cancel_btn", fileChooserCancelBtn);
    builder->get_widget("filechooser_open_btn", fileChooserOpenBtn);
    builder->get_widget("filechooser_current_file", fileChooserCurrentFile);

    // Connect all the buttons to their event handlers
    playBtn->signal_clicked().connect(sigc::mem_fun(this, &gtkInterface::playBtnOnClick));
    pauseBtn->signal_clicked().connect(sigc::mem_fun(this, &gtkInterface::pauseBtnOnClick));
    stopBtn->signal_clicked().connect(sigc::mem_fun(this, &gtkInterface::stopBtnOnClick));
    browseBtn->signal_clicked().connect(sigc::mem_fun(this, &gtkInterface::browseBtnOnClick));
    addFileBtn->signal_clicked().connect(sigc::mem_fun(this, &gtkInterface::addFileBtnOnClick));
    prevBtn->signal_clicked().connect(sigc::mem_fun(this, &gtkInterface::prevBtnOnClick));
    nextBtn->signal_clicked().connect(sigc::mem_fun(this, &gtkInterface::nextBtnOnClick));
    fileChooserOpenBtn->signal_clicked().connect(sigc::mem_fun(this, &gtkInterface::fOpenBtnOnClick));
    fileChooserCancelBtn->signal_clicked().connect(sigc::mem_fun(this, &gtkInterface::fCancelBtnOnClick));
    fileChooserDialog1->signal_selection_changed().connect(sigc::mem_fun(this, &gtkInterface::fFileOnChanged));
}

gtkInterface::~gtkInterface()
{
    delete updateUiThread;
}

void gtkInterface::updateProgressBar()
{
   //progressBar->set_fraction(1.0f/((aManager->getAudioLength()/aManager->getAudioPosition())));
    std::cout << aManager->getAudioLength();
}

// Button event handlers
void gtkInterface::playBtnOnClick()
{
    if(aManager->getState() != int(audioManager::audioStates::PLAYING) && aManager->audioQueue.size() > 0)
    {
        if(aManager->getState() == int(audioManager::audioStates::STOPPED))
        {
            aManager->setStream(aManager->audioQueue.getCurrentQueueItem());
        }
        aManager->setState(int(audioManager::audioStates::PLAY));
        updateUiThread = new std::thread(&gtkInterface::updateProgressBar, this);
    }
}

void gtkInterface::pauseBtnOnClick()
{
    aManager->setState(int(audioManager::audioStates::PAUSE));
}

void gtkInterface::stopBtnOnClick()
{
    aManager->setState(int(audioManager::audioStates::STOP));
}

void gtkInterface::browseBtnOnClick()
{
    fileChooserDialog1->show();
}

void gtkInterface::addFileBtnOnClick()
{
    aManager->audioQueue.add(pathEntry->get_text());
}

void gtkInterface::prevBtnOnClick()
{
    aManager->setState(int(audioManager::audioStates::STOP));
    aManager->audioQueue.prev();
}

void gtkInterface::nextBtnOnClick()
{
    aManager->setState(int(audioManager::audioStates::STOP));
    aManager->audioQueue.next();
}

void gtkInterface::fOpenBtnOnClick()
{
    aManager->audioQueue.add(fileChooserDialog1->get_filename());
    pathEntry->set_text(fileChooserDialog1->get_filename());
    fileChooserDialog1->hide();
}

void gtkInterface::fCancelBtnOnClick()
{
    fileChooserDialog1->hide();
}

void gtkInterface::fFileOnChanged()
{
    fileChooserCurrentFile->set_text(fileChooserDialog1->get_filename());
}
/**
 *****************************************************************************************
 *  @brief      Sets up the interface for running
 *
 *  @usage      This function sets up the interface so it can be used in the GMainLoop
 *
 *  @param      int argc: Holds the total number of arguments passed to the function
 *  @param      char **argv: Hold the arguments passed to the function
 ******************************************************************************************
 **/
int gtkInterface::run(int argc, char **argv)
{
    _argc = argc;
    _argv = argv;
    aManager = new audioManager(_argc, _argv);
    window->show();
}