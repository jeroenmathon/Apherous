//
// Created by jeroen on 9/24/15.
//

#include "audio_mgr.h"

/**
 *****************************************************************************************
 *  @brief      The gst bus callback.
 *
 *  @usage      This function collects data from the GLIB thread that manages the bus
 *
 *  @param      GstBus *bus: A pointer to the gst bus
 *  @param      GstMessage *msg: A pointer to the GstMessage object that holds information returned by the bus thread
 *  @param      gpointer data: the a pointer to the data that will be used in the thread
 ******************************************************************************************
 **/
static gboolean bus_call(GstBus *bus, GstMessage *msg, gpointer data)
{
    GMainLoop *loop = (GMainLoop *) data;

    switch(GST_MESSAGE_TYPE(msg))
    {
        case GST_MESSAGE_EOS:
        {
            g_print("End of stream\n");
            g_main_loop_quit(loop);
            break;
        }

        case GST_MESSAGE_ERROR:
        {
            gchar *debug;
            GError *error;

            gst_message_parse_error(msg, &error, &debug);
            g_free(debug);

            g_printerr("Error: %s\n", error->message);
            g_free(error);

            g_main_loop_quit(loop);
            break;
        }

        default:
        {
            break;
        }
    }

    return TRUE;
}

audioManager::audioManager(int argc, char  **argv)
{
    pipeline = gst_element_factory_make("playbin", NULL); // Initialize the pipeline
    gst_element_set_state(pipeline, GST_STATE_NULL);      // Set state to NULL
    loop = g_main_loop_new(NULL, FALSE);                  // Create a new loop
    bus = gst_pipeline_get_bus(GST_PIPELINE(pipeline));   // Initialize the asynchronous message bus
    bus_watch_id = gst_bus_add_watch(bus, bus_call, loop);// Add the asynchronous message bus to the main loop
    gst_init(&argc, &argv);                               // Initialize GStreamer
    setStream("EMPTY");
}

audioManager::~audioManager()
{
    // Clean up objects
    gst_message_unref(msg);
    gst_object_unref(bus);
    gst_element_set_state(pipeline, GST_STATE_NULL);
    gst_object_unref(pipeline);
    g_source_remove(bus_watch_id);
    g_main_loop_unref(loop);
}

/**
 *****************************************************************************************
 *  @brief      Sets the state of the audio player
 *
 *  @usage      This function can be called from the audioManager class
 *
 *  @param      int state: The state top put the audio player in.(Being PLAY,PAUSE or STOP)
 ******************************************************************************************
 **/
void audioManager::setState(int state)
{
    switch(state)
    {
        case int(audioStates::PLAY):    // Sets audio state to play
        {
            gst_element_set_state(pipeline, GST_STATE_PLAYING);
            break;
        }

        case int(audioStates::PAUSE):   // Sets audio state to pause
            gst_element_set_state(pipeline, GST_STATE_PAUSED);
            break;

        case int(audioStates::STOP):    // Sets audio state to stop
            gst_element_set_state(pipeline, GST_STATE_NULL);
            break;

        default:
            break;
    }
}

/**
 *****************************************************************************************
 *  @brief      Returns the state of the player
 *
 *  @usage      Returns what state the audio player is currently in
 *
 *  @return     int: The state the audio player is in
 ******************************************************************************************
 **/
int audioManager::getState()
{
    GstState tmpState;
    if(gst_element_get_state(pipeline, &tmpState, NULL, GST_CLOCK_TIME_NONE))
    if(tmpState == GST_STATE_PLAYING) return int(audioStates::PLAYING);
    else if(tmpState == GST_STATE_PAUSED) return int(audioStates::PAUSED);
    else if(tmpState == GST_STATE_NULL) return int(audioStates::STOPPED);

    return -1;
}
/**
 *****************************************************************************************
 *  @brief      Sets the stream for the audio manager
 *
 *  @usage      This sets the audio stream/file to use for the audio player
 *
 *  @param      std::string uri: The URI used for the audio player
 ******************************************************************************************
 **/
void audioManager::setStream(std::string uri)
{
    // Assemble command
    std::stringstream uriStream;
    uriStream << "playbin uri=\"" << uri << "\"";

    // Set stream to uri
    pipeline = gst_parse_launch(uriStream.str().c_str(), &gError);
    if(gError != nullptr)
    {
        std::cout << "Glib Error:" << gError->message << "\n";
    }
}

/**
 *****************************************************************************************
 *  @brief      Returns the length of a song
 *
 *  @return     guint64: The length if a song
 ******************************************************************************************
 **/
guint64 audioManager::getAudioLength()
{
    gint64 len = -1;
    gst_element_query_duration(pipeline, GST_FORMAT_TIME, &len);
    return len;
}

/**
 *****************************************************************************************
 *  @brief      Returns the current position in a song
 *
 *  @return     guint64: The position if a song
 ******************************************************************************************
 **/
guint64 audioManager::getAudioPosition()
{
    gint64 pos = -1;
    gst_element_query_position(pipeline, GST_FORMAT_TIME, &pos);
    return pos/GST_SECOND;
}
