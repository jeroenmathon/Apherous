//
// Created by jeroen on 9/26/15.
//

#ifndef APHEROUS_UI_GTK_H
#define APHEROUS_UI_GTK_H

#include <iostream>
#include <gtkmm.h>
#include <sstream>
#include <thread>
#include "audio_mgr.h"

class gtkInterface
{
public:
    // Constructor and Destructor
    gtkInterface();
    ~gtkInterface();

    // Public routines
    int run(int argc, char **argv);

    // Public variables
    Gtk::Window *window;

private:
    // Private routines

    // Button event handlers
    void playBtnOnClick();      void pauseBtnOnClick();
    void stopBtnOnClick();      void browseBtnOnClick();
    void addFileBtnOnClick();   void prevBtnOnClick();
    void nextBtnOnClick();

    // Filechooser event handlers
    void fOpenBtnOnClick();     void fCancelBtnOnClick();
    void fFileOnChanged();

    // Private variables
    Glib::RefPtr<Gtk::Builder> builder = Gtk::Builder::create_from_file("ui/ui.glade");

    Gtk::Box *mainBox;

    // Control box
    Gtk::Box *controlBox;
    Gtk::Button *playBtn;
    Gtk::Button *pauseBtn;
    Gtk::Button *stopBtn;
    Gtk::Button *prevBtn;
    Gtk::Button *nextBtn;
    Gtk::ProgressBar *progressBar;

    // Filebox
    Gtk::Box *fileBox;
    Gtk::Button *browseBtn;
    Gtk::Button *addFileBtn;
    Gtk::Entry *pathEntry;

    // Current songbox
    Gtk::Box *currentSongBox;
    Gtk::Label *currentSongText;
    Gtk::Label *currentSong;

    // Filechooser
    Gtk::FileChooserDialog *fileChooserDialog1;
    Gtk::ButtonBox *fileChooserDialogActionArea1;
    Gtk::Entry *fileChooserCurrentFile;
    Gtk::Button *fileChooserCancelBtn;
    Gtk::Button *fileChooserOpenBtn;
    Gtk::Box *fileChooserDialogVbox;

    std::thread *updateUiThread;
    void updateProgressBar();
    GMainLoop *loop;         // The GLIB's main loop to be used in the class.
    audioManager *aManager;
    int _argc;
    char **_argv;
};
#endif //APHEROUS_UI_GTK_H
