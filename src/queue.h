//
// Created by jeroen on 9/28/15.
//

#ifndef APHEROUS_QUEUEMANAGER_H
#define APHEROUS_QUEUEMANAGER_H

#include <iostream>
#include <vector>
#include <sstream>
#include <fstream>

/// queue
/** This class handles the queue **/
class queue
{
public:
    // Public routines
    void add(std::string uri);          // Adds a uri to the audioQueue
    void next();                        // Increments currentQueueItem
    void prev();                        // Decrements currentQueueItem
    int size();                         // Returns size of the audioQueue
    int at();                           // Returns the current position of the audioQueue
    std::string getCurrentQueueItem();  // Retrieves the current uri from the audioQueue

private:
    // Private variables
    std::vector<std::string> uriQueue;  // The vector holding the uri's
    int currentQueueItem = 0;           // The current item in audioQueue
};

#endif //APHEROUS_QUEUEMANAGER_H
