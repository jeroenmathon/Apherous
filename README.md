# Apherous
###Welcome to Apherous.
Apherous is a Music player which will also allow users to share their library on a local network via streaming.
Apherous will also feature navigation by genre(Something most linux music players either hide or lack).

### Compiling
Compiling is quite straight forward.
Make a build directory, enter it and then run `cmake ../ ; make ` A executable called "Apherous" should appear in the build directory.

Dependencies for compiling Apherous are currently.
  `libgtkmm-3.0-dev`
  `libgtkmm-3.0-dev`

### Join the project!
Feel free to fork and make pull requests, Every little bit of help is greatly appriciated!

### Development
Development information can be found on [this page](https://github.com/JeroenMathon/Apherous/wiki/Development)

### Documentation
The documentation of this project can be found [here](http://jeroenmathon.github.io/Apherous/doxy/html/)
